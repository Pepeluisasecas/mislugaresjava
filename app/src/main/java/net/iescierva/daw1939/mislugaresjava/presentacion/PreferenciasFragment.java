package net.iescierva.daw1939.mislugaresjava.presentacion;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.iescierva.daw1939.mislugaresjava.R;

public class PreferenciasFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}