package net.iescierva.daw1939.mislugaresjava.modelo;

public class Lugar {
    private String nombre;
    private String direccion;
    private TipoLugar tipo;
    private int telefono;
    private String url;
    private String comentario;
    private final long fecha;
    private float valoracion;
    private final GeoPunto posicion;
    public Lugar(String nombre, String direccion, double latitud, double longitud,
                 TipoLugar tipo, int telefono, String url, String comentario,
                 int valoracion) {
        this.fecha = System.currentTimeMillis();
        this.posicion = new GeoPunto(latitud, longitud);
        this.tipo = tipo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.url = url;
        this.comentario = comentario;
        this.valoracion = valoracion;
    }

    public Lugar() {
        this("","",0,0,TipoLugar.OTROS,0,"","",0);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public long getFecha() {
        return fecha;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }
}