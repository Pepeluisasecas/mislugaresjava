package net.iescierva.daw1939.mislugaresjava.modelo;

import androidx.annotation.NonNull;

public class GeoPunto {

    private final double longitud;
    private final double latitud;

    public GeoPunto(double longitud, double latitud) {
        this.longitud= longitud;
        this.latitud= latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    @NonNull
    @Override
    public String toString() {
        return "GeoPunto{" +
                "longitud=" + longitud +
                ", latitud=" + latitud +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoPunto geoPunto = (GeoPunto) o;

        if (Double.compare(geoPunto.getLongitud(), getLongitud()) != 0) return false;
        return Double.compare(geoPunto.getLatitud(), getLatitud()) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getLongitud());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLatitud());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
