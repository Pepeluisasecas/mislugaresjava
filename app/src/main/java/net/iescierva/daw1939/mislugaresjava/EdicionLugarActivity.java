package net.iescierva.daw1939.mislugaresjava;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.daw1939.mislugaresjava.casos_uso.CasosUsoLugar;
import net.iescierva.daw1939.mislugaresjava.datos.RepositorioLugares;
import net.iescierva.daw1939.mislugaresjava.modelo.Lugar;
import net.iescierva.daw1939.mislugaresjava.modelo.TipoLugar;

public class EdicionLugarActivity extends AppCompatActivity {

    private CasosUsoLugar usoLugar;
    private int pos;
    private Lugar lugar;
    private EditText nombre;
    private Spinner tipo;
    private EditText direccion;
    private EditText telefono;
    private EditText url;
    private EditText comentario;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edicion_lugar);
        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("pos", 0);
        RepositorioLugares lugares = ((Aplicacion) getApplication()).getLugares();
        usoLugar = new CasosUsoLugar(this, lugares);
        lugar = lugares.get_element(pos);

        tipo = findViewById(R.id.spinnerTipo);
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, TipoLugar.getNombres());
        adaptador.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        tipo.setAdapter(adaptador);
        tipo.setSelection(lugar.getTipo().ordinal());


        actualizaVistas();

            telefono = (EditText) findViewById(R.id.editTextTelefono);
            telefono.setText(Integer.toString(lugar.getTelefono()));

    }

    @SuppressLint("SetTextI18n")
    public void actualizaVistas() {
        nombre = findViewById(R.id.editTextNombre);
        nombre.setText(lugar.getNombre());
        direccion = findViewById(R.id.editTextDireccion);
        direccion.setText(lugar.getDireccion());
        telefono = findViewById(R.id.editTextTelefono);
        telefono.setText(Integer.toString(lugar.getTelefono()));
        url = findViewById(R.id.editTextUrl);
        url.setText(lugar.getUrl());
        comentario = findViewById(R.id.editTextComentario);
        comentario.setText(lugar.getComentario());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editar_lugar_menu, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_guardar:
                lugar.setNombre(nombre.getText().toString());
                lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
                lugar.setDireccion(direccion.getText().toString());
                lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
                lugar.setUrl(url.getText().toString());
                lugar.setComentario(comentario.getText().toString());
                usoLugar.guardar(pos, lugar);
                finish();

                return true;
            case R.id.accion_cancelar:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
