package net.iescierva.daw1939.mislugaresjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Actividad2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad2);
        TextView resultText = findViewById(R.id.textViewResultado);
        Double resultado = getIntent().getDoubleExtra("resultado",0);

        resultText.setText(resultado.toString());
    }
}