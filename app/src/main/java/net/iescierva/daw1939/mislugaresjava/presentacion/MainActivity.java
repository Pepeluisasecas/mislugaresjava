package net.iescierva.daw1939.mislugaresjava.presentacion;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import net.iescierva.daw1939.mislugaresjava.Actividad1;
import net.iescierva.daw1939.mislugaresjava.Aplicacion;
import net.iescierva.daw1939.mislugaresjava.CrearLugarActivity;
import net.iescierva.daw1939.mislugaresjava.R;
import net.iescierva.daw1939.mislugaresjava.VistaLugarActivity;
import net.iescierva.daw1939.mislugaresjava.casos_uso.CasosUsoLugar;
import net.iescierva.daw1939.mislugaresjava.datos.RepositorioLugares;

public class MainActivity extends AppCompatActivity {
    private CasosUsoLugar usoLugar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bAcercaDe = findViewById(R.id.button03);
        bAcercaDe.setOnClickListener(view -> lanzarAcercaDe(null));

        Button bPreferencias = findViewById(R.id.button02);
        bPreferencias.setOnClickListener(view -> lanzarPreferencias(null));

        Button bCrear = findViewById(R.id.button09);
        bCrear.setOnClickListener(view -> lanzarVistaCreaLugar(null));

        Button bActividad1 = findViewById(R.id.buttonActividad1);
        bActividad1.setOnClickListener(view1 -> lanzarActicidad1(null));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolBarLayout.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        RepositorioLugares lugares = ((Aplicacion) getApplication()).getLugares();
        usoLugar = new CasosUsoLugar(this, lugares);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            lanzarPreferencias(null);
            return true;
        } else if (id == R.id.acercaDe) {
            lanzarAcercaDe(null);
            return true;
        }
        if (id == R.id.menu_buscar) {
            lanzarVistaLugar(null);
            return true;
        }
        if (id == R.id.menu_crear){
            lanzarVistaCreaLugar(null);
        }
        if (id == R.id.menu_actividad1){
            lanzarActicidad1(null);
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    public void mostrarPreferencias(View view) {
        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(this);
        String s = "notificaciones: " + pref.getBoolean("notificaciones", true)
                + ", e-mail: " + pref.getString("email", "")
                + ", tiposNotificaciones: " + pref.getString("tiposNotificaciones", "0")
                + ", máximo a mostrar: " + pref.getString("maximo", "?")
                + ", orden: " + pref.getString("orden", "");
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }*/

    public void lanzarAcercaDe(View view) {
        Intent i = new Intent(this, AcercaDeActivity.class);
        startActivity(i);
    }

    public void lanzarPreferencias(View view) {
        Intent i = new Intent(this, PreferenciasActivity.class);
        startActivity(i);
    }

    public void lanzarVistaLugar(View view) {
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        new AlertDialog.Builder(this)
                .setTitle("Selección de lugar")
                .setMessage("indica su id:")
                .setView(entrada)
                .setPositiveButton("Ok", (dialog, whichButton) -> {
                    int id = Integer.parseInt(entrada.getText().toString());
                    usoLugar.mostrar(id);
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }

    public void lanzarActicidad1(View view) {
        Intent i = new Intent(this, Actividad1.class);
        this.startActivity(i);
    }


    public void lanzarVistaCreaLugar(View view){
        usoLugar.crearLugar();
    }

}