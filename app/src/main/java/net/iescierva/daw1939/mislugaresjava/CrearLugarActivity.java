package net.iescierva.daw1939.mislugaresjava;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import net.iescierva.daw1939.mislugaresjava.casos_uso.CasosUsoLugar;
import net.iescierva.daw1939.mislugaresjava.datos.RepositorioLugares;
import net.iescierva.daw1939.mislugaresjava.modelo.Lugar;
import net.iescierva.daw1939.mislugaresjava.modelo.TipoLugar;

public class CrearLugarActivity extends AppCompatActivity {

    private EditText nombre;
    private Spinner tipo;
    private EditText direccion;
    private EditText telefono;
    private EditText url;
    private EditText comentario;
    private CasosUsoLugar usoLugar;
    private Lugar lugar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_lugar);

        RepositorioLugares lugares = ((Aplicacion) getApplication()).getLugares();
        usoLugar = new CasosUsoLugar(this, lugares);
        nombre = findViewById(R.id.editTextNombre);
        tipo = findViewById(R.id.spinnerTipo);

        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, TipoLugar.getNombres());
        adaptador.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        tipo.setAdapter(adaptador);

        direccion = findViewById(R.id.editTextDireccion);
        telefono = findViewById(R.id.editTextTelefono);
        url = findViewById(R.id.editTextUrl);
        comentario = findViewById(R.id.editTextComentario);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editar_lugar_menu, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_guardar:
                Lugar lugar = new Lugar();
                lugar.setNombre(nombre.getText().toString());
                lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
                lugar.setDireccion(direccion.getText().toString());
                lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
                lugar.setUrl(url.getText().toString());
                lugar.setComentario(comentario.getText().toString());
                usoLugar.anadirLugar(lugar);
                finish();
                return true;
            case R.id.accion_cancelar:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}