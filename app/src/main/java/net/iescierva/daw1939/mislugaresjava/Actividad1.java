package net.iescierva.daw1939.mislugaresjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Actividad1 extends AppCompatActivity {

    EditText editTextNumero1;
    EditText editTextNumero2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad1);
        editTextNumero1 = findViewById(R.id.editTextNumero1);
        editTextNumero2 = findViewById(R.id.editTextNumero2);
    }


    public void cambiarNumeros(View view) {
        String aux = editTextNumero1.getText().toString();
        editTextNumero1.setText(editTextNumero2.getText().toString());
        editTextNumero2.setText(aux);
    }

    public void sumar(View view) {
        Intent i = new Intent(this,Actividad2.class);
        double numero1 = Double.parseDouble(editTextNumero1.getText().toString());
        double numero2 = Double.parseDouble(editTextNumero2.getText().toString());
        double suma = numero1+numero2;
        i.putExtra("resultado",suma);
        this.startActivity(i);
    }
}