package net.iescierva.daw1939.mislugaresjava;

import android.app.Application;

import net.iescierva.daw1939.mislugaresjava.datos.LugaresLista;
import net.iescierva.daw1939.mislugaresjava.datos.RepositorioLugares;

public class Aplicacion extends Application {

    private final RepositorioLugares lugares = new LugaresLista();
    @Override public void onCreate() {
        super.onCreate();
    }

    public RepositorioLugares getLugares() {
        return lugares;
    }
}