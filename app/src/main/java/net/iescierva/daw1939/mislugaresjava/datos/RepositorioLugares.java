package net.iescierva.daw1939.mislugaresjava.datos;

import net.iescierva.daw1939.mislugaresjava.modelo.Lugar;

public interface RepositorioLugares { Lugar get_element(int id); //Devuelve el elemento dado su id
    void add(Lugar lugar); //Añade el elemento indicado

    void delete(int id); //Elimina el elemento con el id indicado

    void update_element(int id, Lugar lugar); //Reemplaza un elemento
}
