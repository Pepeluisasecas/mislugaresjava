package net.iescierva.daw1939.mislugaresjava.casos_uso;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;

import net.iescierva.daw1939.mislugaresjava.CrearLugarActivity;
import net.iescierva.daw1939.mislugaresjava.EdicionLugarActivity;
import net.iescierva.daw1939.mislugaresjava.VistaLugarActivity;
import net.iescierva.daw1939.mislugaresjava.datos.RepositorioLugares;
import net.iescierva.daw1939.mislugaresjava.modelo.Lugar;

public class CasosUsoLugar {
    private final Activity actividad;
    private final RepositorioLugares lugares;

    public CasosUsoLugar(Activity actividad, RepositorioLugares lugares) {
        this.actividad = actividad;
        this.lugares = lugares;
    }

    // OPERACIONES BÁSICAS
    public void mostrar(int pos) {
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivity(i);
    }

    public void borrar(final int id) {
        lugares.delete(id);
        actividad.finish();
    }

    public void borrarLugar(final int id) {
        new AlertDialog.Builder(actividad)
                .setTitle("Borrado de lugar")
                .setMessage("¿Estás seguro de que quieres eliminar este lugar?")
                .setPositiveButton("Confirmar", (dialog, whichButton) -> {
                    borrar(id);
                    actividad.finish();
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }
    public void guardar(int id, Lugar nuevoLugar) {
        lugares.update_element(id, nuevoLugar);
    }

    public void editar(int pos, int codigoSolicitud) {
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivityForResult(i, codigoSolicitud);
    }
    public void crearLugar(){
        Intent i = new Intent(actividad, CrearLugarActivity.class);
        actividad.startActivity(i);
    }

    public void anadirLugar(Lugar lugarAnadir){
        lugares.add(lugarAnadir);
    }

}